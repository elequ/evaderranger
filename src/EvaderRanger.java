import java.awt.Color;
//import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * EvaderRanger
 * 
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class EvaderRanger implements ActionListener {

	final static Integer xcanvas = 1005, ycanvas = 795;
	final static int scale = 15;
	final static int delay = 25; // milliseconds

	private GameComponent canvas;
	private Game game;
	private Timer timer;
	private JFrame jframe;

	public EvaderRanger() {
		jframe = new JFrame("Evader Game");
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas = new GameComponent(xcanvas, ycanvas);
		(jframe.getContentPane()).add(canvas);
		timer = new Timer(delay, this);
		jframe.pack();
		jframe.setVisible(true);
	}

	public void runGame() {
		System.out.println("You are Green. To win catch Red. Don't get touched by Orange.");
		System.out.println("Start Game.");
		game = new Game((double) xcanvas, (double) ycanvas);
		timer.start();
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == timer) {
			if (!canvas.qPress && !game.won) {
				canvas.clearOffscreen();
				game.draw(canvas);
				canvas.drawOffscreen();
				game.step(canvas);
			} else {
				System.out.println("Game Over");
				timer.stop();
				// jframe.dispose();
			}
		}
	}
	
	private void setbackground() {
		Graphics g = canvas.getBackgroundGraphics();
		g.fillRect(0, 0, xcanvas, ycanvas);
	}

	public static void main(String[] args) {
		EvaderRanger sim;
		sim = new EvaderRanger();	
		sim.runGame();
	}
}

