/**
 * EvaderRanger
 * 
 * main author: Elena Williams
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public abstract class Agent {
	public Double x, y;
	boolean [][] array;
	int scale;
	public Agent(){}

	public Agent(boolean [][] array, int scale, Double xp, Double yp) {
		//this.scale = scale;	
		this.x = xp;
		this.y = yp;
		this.array = array;
		this.scale = scale;
	}
	
	public Boolean collide(Agent b) {
		return pointIn(b.x, b.y) || pointIn(b.x + b.width(), b.y)
				|| pointIn(b.x, b.y + b.height())
				|| pointIn(b.x + b.width(), b.y + b.height())
				|| b.pointIn(x, y);
	}
	
	public Boolean pointIn(Double xp, Double yp) {
		return x < xp && xp < x + width() && y < yp && yp < y + height();
	}

	public static int mapx(Game h, GameComponent canvas, double x) {
		return (int) Math.round(x);
	}

	public static int mapy(Game h, GameComponent canvas, double y) {
		return (int) Math.round(y);
	}

	// This will help the players and bandits check if their positions are on the walls
	public void collideGame(Double xVel, Double yVel) {
		System.out.println("inside collide game");
		
		int posx = (int) Math.round(x/scale);
		int posy = (int) Math.round(y/scale);
		
		if(array[posx+1][posy]==true && xVel > 0){
			xVel = -xVel;
		}
	 	if(array[posx-1][posy] == true && xVel < 0){
	 		xVel = -xVel;
	 	}
		if(array[posx][posy+1]==true && yVel > 0){
			yVel = -yVel;
		}
		if(array[posx][posy-1] == true && yVel < 0){
			yVel = -yVel;
		}
		if(xVel > 3)xVel = 3.0;
		if(xVel < -3)xVel = -3.0;
		if(yVel > 3)yVel = 3.0;
		if(yVel < -3)yVel = -3.0;
	}
	
	/*
	public float headingTo(){
		
		
	}
	*/
	//runFrom
	//runTo
	
	public abstract void draw(Game h, GameComponent canvas);
	public abstract void step(Game bw, GameComponent canvas);
	public abstract Double width();
	public abstract Double height();
}
