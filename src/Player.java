import javax.swing.ImageIcon;

/**
 * EvaderRanger
 * 
 * Quazi Hameem Mahmud
 * Iain Watson Smith
 * Elena Williams
 * 
 * Semester 2, 2011 Assignment
 * Final Due: 19-Oct-2011
 */

public class Player extends Agent {
	static final ImageIcon player = new ImageIcon(EvaderRanger.class.getResource("player.jpg"));
	static final Integer width = player.getIconWidth();
	static final Integer height = player.getIconHeight();
	static int velocity;
	static Double xVel = 2.0;
	static Double yVel = 2.0;
	boolean [][] array;
	static int scale;

	public Player(boolean [][] array, int scale, Double xp, Double yp) {
		super();
		this.x = xp;
		this.y = yp;
		this.array = array;
		this.scale = scale;
		xVel = 2.0;
		yVel = 2.0;
		
	}
	
	//Painted the image so that (x,y) is in the center of the icon. Width 

	public void draw(Game h, GameComponent canvas) {
		canvas.drawImage(player.getImage(), mapx(h, canvas, (x-(width/3)-5.0)),
				mapy(h, canvas, y-(height/3)-5.0));
	}

	//place to step the player, added the walls as an array, from canvas get board
	
	public void step(Game bw, GameComponent canvas) {

		// Change the velocity depending on the press of the keys
		if(canvas.rPress){
			xVel += 0.1;
		}
		if(canvas.lPress){
			xVel -= 0.1;
		}
		if(canvas.dPress){
			yVel += 0.1;
		}
		if(canvas.uPress){
			yVel -= 0.1;
		}
		
		// Update and set the new velocities depending on collision with wall
		//Double [] velocities = collideGame(xVel, yVel, x, y);;
		
		//xVel = velocities[0];
		//yVel = velocities[1];
		collideGame(xVel,yVel);

		x += xVel;
		y += yVel;
	}

	public Double width() {
		return (double) width;
	}

	public Double height() {
		return (double) height;
	}
}
